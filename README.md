csv
===

The describes the automated sftp send of csv file to Epsilon.

Epsilon crontab process has been setup.
This runs every day at 9am, 1pm, 6pm. 

MAILTO=lake42@gmail.com,paul.barrick@infosurv.com
0 9,13,18 * * * /var/www/epsilon/csv/csv.sh >> /var/www/epsilon/csv/csv.log

The above cronjob is setup on presentation under user:james

The app code lives here:
/var/www/epsilon/csv

on presentation

csv.sh has everything in it. no need to stop the cron job, just comment out all the lines in csv.sh.