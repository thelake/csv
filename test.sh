#!/usr/bin/expect -f

# connect via scp
spawn scp 'james@69.56.173.240:/var/www/html/jlpd/token/' test.log
#######################
expect {
  -re ".*es.*o.*" {
    exp_send "yes\r"
    exp_continue
  }
  -re ".*sword.*" {
    exp_send "ronky123\r"
  }
}
interact
