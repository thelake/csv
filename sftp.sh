#!/usr/bin/expect

# [usage] ./sftp.sh [host] [user] [pass] [action] [path] [file_data]
# [usage] ./sftp.sh ssh.benow.com METM_Infosurv g$RvKa9U TEST /Incoming/TEST/ sftplist.log
# [usage] ./sftp.sh jlproductions.net james ronky123 TEST /csv/ sftplist.log


set host [lindex $argv 0]
set user [lindex $argv 1]
set pass [lindex $argv 2]
set action [lindex $argv 3]
set path [lindex $argv 4]
set file_data [lindex $argv 5]
set data [split $file_data "\n"] 
puts "Starting...."  

puts "\"$host\""
set fileopen [open $file_data "r"]
puts $file_data
set d [read $fileopen]
#puts $d
puts $user@$host
spawn sftp $user@$host  

expect "password:"   
send "$pass\r"  
interact
#expect "sftp>"
#send "ls\r"  
#send "cd $path\r" 
#puts "just moved into position"

#if {$action == "TEST"} {
#    # Do something
#    set fileopen [open $file_data "r"]
#	set d [read $fileopen]    
#    expect"sftp>"
#    send "mput $d\r"
#} else {
#    puts "Can not write the file on remote server."
#}

#expect "sftp>"  
#send_user "quit\r"  

#puts "DONE....."
